#!/bin/bash

RED='\033[0;31m'
NC='\033[0m' # No Color
printf "\n Client init , make ${RED}general configuration${NC} \n"

kubectl apply -f namespace.yaml
#kubectl apply -f secret.yaml

# install microservice step by step

printf "\n start deploy ${RED}WebServer${NC} \n"
cd webserver && bash install.sh

