.PHONY: ls build clean generate

ls:
	ls -la

clean:
	rm -rf ./bin

config:
	export KUBECONFIG=~/.kube/becoming-non-prod.yaml

generate: config 	
	.do/generate.sh $(client)
