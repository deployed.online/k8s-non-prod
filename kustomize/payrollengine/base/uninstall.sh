#!/bin/bash

export KUBECONFIG=~/.kube/becoming-non-prod.yaml

RED='\033[0;31m'
NC='\033[0m' # No Color
printf "\n Client init , make ${RED}general configuration${NC} \n"

kubectl delete -f namespace.yaml
kubectl delete -f secret.yaml

# install microservice step by step

printf "\n start remove ${RED}portal${NC} \n"
cd portal && bash uninstall.sh

#printf "\n start deploy ${RED}authn${NC} \n"
#cd ../authn_service && bash install.sh

#printf "\n start deploy ${RED}bic${NC} \n"
#cd ../bic_service && bash install.sh

printf "\n start remove  ${RED}dashboard${NC} \n "
cd ../dashboard_service && bash install.sh

#printf "\n start deploy ${RED}driver${NC} \n"
#cd ../driver_service && bash install.sh

#printf "\n start deploy ${RED}infotype${NC} \n"
#cd ../infotype_service && bash install.sh

#printf "\n start deploy ${RED}slip${NC} \n"
#cd ../slip_service && bash install.sh
