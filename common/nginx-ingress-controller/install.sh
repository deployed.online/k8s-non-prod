#!/usr/bin/env bash

export KUBECONFIG=~/.kube/becoming-non-prod.yaml

# https://docs.ovh.com/gb/en/kubernetes/installing-nginx-ingress/

helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx;
helm repo update;
helm -n ingress-nginx install ingress-nginx ingress-nginx/ingress-nginx --create-namespace;
