#!/usr/bin/env bash

export KUBECONFIG=~/.kube/becoming-non-prod.yaml

# https://docs.ovh.com/gb/en/kubernetes/installing-nginx-ingress/

helm -n ingress-nginx delete ingress-nginx
