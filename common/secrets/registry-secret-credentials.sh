#!/usr/bin/env bash

export KUBECONFIG=~/.kube/becoming-non-prod.yaml

kubectl create secret docker-registry dockerio-credentials \
        --namespace=example \
        --docker-server=docker.io \
        --docker-username=username \
        --docker-password="password" \
        --docker-email=hello@becoming.tech
