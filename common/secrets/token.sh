#!/usr/bin/env bash

export KUBECONFIG=~/.kube/becoming-non-prod.yaml

kubectl -n kubernetes-dashboard create token admin-user
