#!/usr/bin/env bash

export KUBECONFIG=~/.kube/becoming-non-prod.yaml

# https://docs.ovh.com/gb/en/private-registry/using-private-registry-with-kubernetes/

# make sure to have only one authentication in your docker config file
kubectl create secret generic ovhprivaterepo \
    --namespace=example \
    --from-file=.dockerconfigjson=/home/"$USER"/.docker/config.json \
    --type=kubernetes.io/dockerconfigjson
