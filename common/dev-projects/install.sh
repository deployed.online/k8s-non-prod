#!/usr/bin/env bash

export KUBECONFIG=~/.kube/becoming-non-prod.yaml

kubectl apply -f example-ns.yaml
kubectl apply -f dev-ns.yaml
kubectl apply -f test-ns.yaml