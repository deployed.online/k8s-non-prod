#!/usr/bin/env bash

export KUBECONFIG=~/.kube/becoming-non-prod.yaml

helm delete --purge cert-manager \
  --namespace cert-manager

