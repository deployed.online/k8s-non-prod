#!/usr/bin/env bash

export KUBECONFIG=~/.kube/becoming-non-prod.yaml

#https://docs.ovh.com/gb/en/kubernetes/installing-cert-manager/

helm repo add jetstack https://charts.jetstack.io
helm repo update

helm install \
  cert-manager jetstack/cert-manager \
  --namespace cert-manager \
  --create-namespace \
  --version v1.9.1 \
  --set installCRDs=true \
  --set webhook.url.host=cert-manager-webhook \
  --set webhook.securePort=10250