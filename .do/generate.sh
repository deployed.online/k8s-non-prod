#!/bin/sh

PATH="./.do"
client=$1

create_k8s_deployment(){
    
    root='k8s-deployment'
    project_name=$(/usr/bin/cat "$PATH/$client.yaml" | /snap/bin/yq  '.k8s-deployment.name')
    /usr/bin/mkdir -p $root/$project_name

    /usr/bin/cat "$PATH/$client.yaml" | /snap/bin/yq  '.k8s-deployment.general' | /usr/bin/awk -F '-' '{print$2}' > general.txt

    while read general 
    do
#         if $general = 'secret'
#         then
#             /usr/bin/cat >$root/$project_name/$general.yaml <<EOL
# apiVersion: v1
# kind: Secret
# metadata:
#   name: ghrc-${client}-portal
#   namespace: ${client}
# data:
#   .dockerconfigjson: 
# type: kubernetes.io/dockerconfigjson
# EOL
#         fi
        /usr/bin/touch $root/$project_name/$general.yml
    done < general.txt

    /usr/bin/cat "$PATH/$client.yaml" | /snap/bin/yq  '.k8s-deployment.services | keys' | /usr/bin/awk -F '-' '{print$2}' > services.txt

    while read service
    do 
        /usr/bin/mkdir -p 'k8s-deployment'/$project_name/$service
        /usr/bin/cat "$PATH/$client.yaml" | /snap/bin/yq  ".k8s-deployment.services.$service | keys"  | /usr/bin/awk -F: '{print$1}' | /usr/bin/awk -F "-" '{print$2}' > resources.txt
        

        while read resource
        do
            /usr/bin/touch $root/$project_name/$service/$resource.yaml
        done < resources.txt

    done < services.txt
    # /usr/bin/rm -f services.txt resources.txt general.txt
}


GREEN='\033[0;32m' 
RED='\033[0;31m'
NC='\033[0m' # No Color

echo "--> Generate file called"

if [ ! $PATH/$client ] 
then
    printf "\nNeed to pas client, use ${RED} make generate client=NAME${NC} \n "
else
    echo "--> Client: ${1}.yaml"

    if [ ! -f $PATH/$client.yaml ]
    then 
        printf " ${RED} fisier $client.yaml ${NC} lipseste  \n" 
    else 
        #extraxt all services from .yml file and create directory with all file .yaml
        create_k8s_deployment
        
    fi
fi




# cat >base/namespace.yaml <<EOL
# apiVersion: v1
# kind: Namespace
# metadata:
#   name: ${client}
# EOL


# #create client secret
# cat >base/secret.yaml <<EOL
# apiVersion: v1
# kind: Secret
# metadata:
#   name: ghrc-${client}-portal
#   namespace: ${client}
# data:
#   .dockerconfigjson: 
#   type: kubernetes.io/dockerconfigjson
# EOL
