#!/bin/bash

export KUBECONFIG=~/.kube/becoming-non-prod.yaml

kubectl delete -f deployment.yaml
kubectl delete -f service.yaml
#kubectl delete -f ingress.yaml
