#!/bin/bash

export KUBECONFIG=~/.kube/becoming-non-prod.yaml

kubectl apply -f deployment.yaml
kubectl apply -f service.yaml
#kubectl apply -f ingress.yaml
