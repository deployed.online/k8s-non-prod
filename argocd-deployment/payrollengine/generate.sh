#!/bin/bash

#n98vIOFKuwAeXeJm

export KUBECONFIG=~/.kube/becoming-non-prod.yaml


GREEN='\033[0;32m' 
RED='\033[0;31m'
NC='\033[0m' # No Color

client="default"
  
if [[ $# != 1 ]]
then
  echo "introduceti nume client ca paramtru"
  exit 1
else
  echo  -e "Client name: ${RED}$1${NC} "
  client=$1
fi

# if [ -d base ] 
# then mkdir base
# fi

#prepare argoсв base file

#argocd-cm.yaml

if [[ ! -f "base/argocd-cm.yaml" ]]; then
    cp ../../common/argocd/argocd-cm.yaml base/argocd-cm.yaml
    cat >>base/argocd-cm.yaml <<EOL

  accounts.${client}: apiKey, login
  accounts.${client}.enabled: "true"
EOL

   cp base/argocd-cm.yaml ../../common/argocd/

fi

kubectl apply -f base/argocd-cm.yaml



#argocd-cm.yaml

if [[ ! -f "base/rbac-cm.yaml" ]]; then

cp ../../common/argocd/rbac-cm.yaml base/rbac-cm.yaml

cat >>base/rbac-cm.yaml <<EOL

    p, role:${client}, applications, *, *, allow    
    p, role:${client}, clusters, get, *, allow
    p, role:${client}, repositories, *, *, allow
    p, role:${client}, projects, *, *, allow
    p, role:${client}, accounts, *, *, allow
    p, role:${client}, logs, get, *, allow
    
    g, ${client}, role:${client} 
EOL

cp base/rbac-cm.yaml ../../common/argocd/

fi

kubectl apply -f base/rbac-cm.yaml


#create client namespace

cat >base/namespace.yaml <<EOL
apiVersion: v1
kind: Namespace
metadata:
  name: ${client}
EOL

#create client secret

cat >base/secret.yaml <<EOL
apiVersion: v1
kind: Secret
metadata:
  name: ghrc-${client}-portal
  namespace: ${client}
data:
  .dockerconfigjson: ewoJImF1dGhzIjogewoJCSJnaGNyLmlvIjogewoJCQkiYXV0aCI6ICJhWFJ3WVhseWIyeHNaVzVuYVc1bE9tZG9jRjlSWmt4VU4yZE5UVWhJYXpOb1ZWSTFlVFpMTWtreE9WTk5TMWRVWlc4eWEyaERjVGs9IgoJCX0sCgkJImdocmMuaW8iOiB7CgkJCSJhdXRoIjogIlpIWnBjblJ2YzNWQVoyMWhhV3d1WTI5dE9tZG9jRjlOVkROSloyMWtjREoxVEV0QlZVZHdablp6YkUxQlNGVnZTVlY2Ym1Fd1owaHVhMW89IgoJCX0KCX0KfQ==
type: kubernetes.io/dockerconfigjson
EOL


#create repo for client
#argocd proj role list payroll
#argocd proj role add-policy $PROJ $ROLE -a get --permission allow -o '*'

user="deployed.online"
token="fqHtk6bLMvdosy2jL6mx"
repo="https://gitlab.com/deployed.online/k8s-non-prod.git"

cat >base/repo.yaml <<EOL
apiVersion: v1
kind: Secret
metadata:
  name: ${client}-portal
  namespace: argocd
  labels:
    argocd.argoproj.io/secret-type: repository
stringData:
  url: ${repo}
#  password: ${token}
#  username: ${user}
EOL

kubectl apply -f base/repo.yaml



#argocd project

cat >base/project.yaml <<EOL
apiVersion: argoproj.io/v1alpha1
kind: AppProject
metadata:
  name: ${client}
  namespace: argocd
spec:
  sourceRepos:
    #- '*'
    - 'https://gitlab.com/deployed.online/k8s-non-prod.git'
  destinations:
    - namespace: ${client}
      server: https://kubernetes.default.svc
#  roles:
#  - name: read-only
#    description: Read-only privileges to my-project
#    policies:
#    - p, proj:my-project:read-only, applications, get, my-project/*, allow
#    groups:
#    - my-oidc-group
EOL


kubectl apply -f base/project.yaml


#change password
#argocd account update-password --account payroll --new-password asdfgh1!
