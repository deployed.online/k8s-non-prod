#!/usr/bin/env bash

export KUBECONFIG=~/.kube/becoming-non-prod.yaml

kubectl create secret docker-registry yeti-ghcrio-credentials \
        --namespace=meditech-test \
        --docker-server=ghcr.io \
        --docker-username=yetifrombecoming \
        --docker-password="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" \
        --docker-email=yeti@becoming.tech
